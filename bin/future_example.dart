Future<String> creatOrderMessege() async {
  var order = await fetchUserOrder();
  return 'Your order is; $order';
}

Future<String> fetchUserOrder() => Future.delayed(
      const Duration(seconds: 2),
      () => 'Larage Latte',
    );

void main() async {
  print('Fetching user order...');
  print(await creatOrderMessege());
  print('End of create order message');
}
