import 'package:lterable/lterable.dart' as lterable;

void main(List<String> arguments) {
  Iterable<int> iterable = [1, 2, 3];
  int value = iterable.elementAt(1);
  print(value);

  // Set, List -> Ierable
  const foods = ['Saled', 'Popcorn', 'Toast', 'Lasagne', "Mamama"];
  Iterable<String> iterableFoods = foods;
  for (var food in iterableFoods) {
    print(food);
  }

  print('First elemant is ${foods.first}');
  print('Last elemant is ${iterableFoods.last}');

  var foundItem1 = foods.firstWhere((element) {
    return element.length > 5;
  });
  print(foundItem1);
  var foundItem2 = foods.firstWhere((element) => element.length > 5);
  print(foundItem2);

  bool predicate(String item) {
    return item.length > 5;
  }

  var foundItem3 = foods.firstWhere(predicate);
  print(foundItem3);

  var foundItem4 =
      foods.firstWhere((item) => item.length > 10, orElse: () => 'None!');
  print(foundItem4);

  var foundItem5 = foods.firstWhere(
      (item) => item.startsWith('M') && item.contains('a'),
      orElse: () => 'None!');
  print(foundItem5);

  if (foods.any((item) => item.contains("a"))) {
    print('At least on item contains "a"');
  }

  if (foods.every((item) => item.length >= 5)) {
    print('All item have length >= 5');
  }

  var numbers = const [1, -2, 3, 42, 0, 4, 5, 6];
  var evenNumbers = numbers.where((numbers) => numbers.isEven);
  for (var number in evenNumbers) {
    print('$number is even');
  }

  if (evenNumbers.any((numbers) => numbers.isNegative)) {
    print('evenNumbers contains negative');
  }

  var largeNumbers = evenNumbers.where((number) => number > 1000);
  if (largeNumbers.isEmpty) {
    print('largNumbers is empty!');
  }

  var numbersUntilZero = numbers.takeWhile((number) => number != 0);
  print('Numbers until 0; $numbersUntilZero');

  var numbersStaringAtZero = numbers.skipWhile((number) => number != 0);
  print('Numbers starting at 0; $numbersStaringAtZero');

  var numbersByTwo = const [1, -2, 3, 42].map((number) => number * 2);
  print('Numbers; $numbersByTwo');
}
